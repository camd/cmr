.. _bondmin:

Bondmin optimization algorithm
==============================

The Paper:

.. container:: article

    Estefanía Garijo del Río, Sami Kaappa, José A. Garrido Torres,
    Thomas Bligaard and Karsten W. Jacobsen

    Machine Learning with bond information for local structure optimizations
    in surface science

    `Preprint`__

    __ https://arxiv.org/abs/2010.09497


The data can be downloaded or browsed online:

* Download data: :download:`bondmin.db`
* Browse data: comming soon ...

.. contents::


Brief description
-----------------

This database contains the results of the optimization tests with
BondMin and other optimizers (validation and results section of the paper).

The dataset key indicates the test the entry corresponds to. The names of the
datasets are the same as in the paper, and a description of the data set can
be found in the paper in the data sets section.

A script generating these datasets, as well as the code to run the optimizers
can be found at:

https://gitlab.com/egarijo/bondmin


Example
-------

.. literalinclude:: plot.py

.. image:: bondmin.png
