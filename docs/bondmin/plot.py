# creates: bondmin.png
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from ase.db import connect

sns.set(style='whitegrid')
sns.set_context('notebook')

db = connect('bondmin.db')

selection = 'dataset=MS5,formula=Pd8H2O'
df = {'optimizer': [], 'n': []}

for row in db.select(selection):
    if 'hollow' not in row.name:
        continue

    df['optimizer'].append(row.optimizer)
    df['n'].append(row.n)

df = pd.DataFrame(df)


sns.stripplot(x='n', y='optimizer', hue='optimizer',
              data=df, alpha=0.5,
              zorder=1, palette='deep')

sns.pointplot(x='n', y='optimizer', hue='optimizer',
              data=df, linestyles='none',
              palette='dark')

plt.xlim(left=0.)
plt.xlabel('iterations')
plt.ylabel('')
plt.title(r'H${}_2$O@ Pd, hollow')
plt.tight_layout()
plt.savefig('bondmin.png')
plt.close()
