.. _abse3:

Database of ABSe3 materials
===========================

.. container:: article

    Peter Bjørn Jørgensen, Estefanía Garijo del Río,
    Mikkel N. Schmidt and Karsten Wedel Jacobsen

    `Materials property prediction using symmetry-labeled
    graphs as atomic position independent descriptors`__

    Phys. Rev. B. 100, 104114 - Published 26 September 2019

    __ https://dx.doi.org/10.1039/C7EE02702H


.. contents::


The data
--------

* Download database: :download:`abse3.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/abse3>`__


Brief description
-----------------

This database contains the relaxed structures of 5976 ternary selenides.

All entries have the stoichiometry ABSe3, where A and B are transition metals.
We have substituted the A and B metals by all the most common transition
metals in a systematic way. For simplicity, the following elements: Cr, Mn, Fe
and Co, have been avoided.

The prototypes used as initial templates are: hexagonal P63/mmc structure of
BaNiO3, orthorhombic Pnma structure of NH4CdCl3/Sn2S3, monoclinic C2/m FePS3,
monoclinic Pc structure of PbPS3, trigonal R3 structureof MnPSe3 and hexagonal
P61 structure of Al2S3.

The structures have been relaxed using PBEsol, and both the PBE and PBEsol
ground state energies are provided.


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example
-------

Band gaps of TeHfS3 calculated in the different phases:

.. image:: abse3_heatmap.svg
.. literalinclude:: heat_map.py
