# creates: enthalpy.png
import numpy as np
import matplotlib.pyplot as plt
from ase.db import connect

db = connect('abx2.db')
names = sorted(row.short_name for row in db.select(phase='ST'))
x = np.arange(len(names))

plt.figure(figsize=(10, 5))

for phase, mark, color in zip(['KT', 'ST', 'CP'], 'osv', 'rbg'):
    y = []
    dy = []
    for row in db.select(phase=phase, sort='short_name'):
        y.append(row.E_relative_perAtom)
        dy.append(row.E_uncertanty_perAtom)
    print(x, y)
    plt.errorbar(x, y, yerr=dy, color=color, fmt=mark, label=phase)

y = []
dy = []
for row in db.select(phase='ST', sort='short_name'):
    y.append(row.E_hull)
    dy.append(row.E_uncertanty_hull)
plt.errorbar(x, y, yerr=dy, color='k', fmt='x', label=r'$\Delta E$')
plt.xticks(x, names, rotation=70)
for xx in x:
    plt.axvline(xx + 0.5, color='lightgrey')
plt.axhline(0, color='grey')
plt.xlim(-0.5, x[-1] + 0.5)
plt.legend()
plt.ylabel(r'$E$ [eV/atom]')
plt.savefig('enthalpy.png', bbox_inches='tight')
