from pathlib import Path
from ase.db import connect
from gpaw import GPAW
from ase.units import Ha

db = connect('ads1d.db')
ref0 = {}
ref1 = {'H': -8.031387634294557 / 2}
ref1['O'] = -27.164 - 2 * ref1['H']


def add(p, x, y):
    print(p)
    calc = GPAW(p / 'gs.gpw')
    atoms = calc.get_atoms()
    e = atoms.get_potential_energy()
    ef = calc.get_fermi_level()
    v = calc.hamiltonian.vt_sG[0]
    wf = v[0].mean() * Ha - ef
    symbols = atoms.symbols
    if 'H' in symbols or 'O' in symbols:
        adsorbates = [s for s in symbols if s in ['H', 'O']]
        m = len(adsorbates)
        adsorbate = adsorbates[0]
        n = (len(atoms) - m) // 4
        coverage = m / n
        eads = (e - ref0[(x, y)] * n - ref1[adsorbate] * m) / m
        extra = {'adsorbate': adsorbate,
                 'coverage': coverage,
                 'eads': eads}
        z = adsorbate + ('' if m == 1 else str(m))
    else:
        n = len(atoms) // 4
        ref0[(x, y)] = e / 2
        extra = {}
        z = ''

    db.write(atoms,
             x=x,
             y=y,
             n=n,
             workfunction=wf,
             folder=str(p),
             uid=f'{x}{n}{y}{n * 3}{z}',
             **extra)


d = Path('/home/niflheim/hadeelm/workflow/Jan/rattle_structure/group_1')
for p in d.glob('??2*6*'):
    x, y = p.name.split('2', 1)
    y = y.split('6')[0]
    print(x, y)
    add(p, x, y)
    add(p / 'brint/model_1', x, y)
    add(p / 'brint/model_1/new', x, y)
    add(p / 'brint/model_1/2x_new', x, y)
    add(p / 'brint/model_1/4x_new', x, y)
    add(p / 'O/model_1', x, y)
    add(p / 'O/model_1/new', x, y)
    add(p / 'O/model_1/2x_new', x, y)
