.. _ads1d:

======================================================================
Chemisorption of gas atoms on one-dimensional transition-metal halides
======================================================================

.. container:: article

    `Chemisorption of gas atoms on one-dimensional transition-metal halides`__

    Hadeel Moustafa, Jens Jørgen Mortensen, Jan Rossmeisl,
    and Karsten Wedel Jacobsen

    arXiv: ... (cond-mat)

    __ https://arxiv.org/abs/2207.05353xxxxxx


* `Browse data <https://cmrdb.fysik.dtu.dk/ads1d>`_
* Download database: :download:`ads1d.db`


.. contents::

Brief description
=================

...


Key-value pairs
===============

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Figure 5
========

Here is an example creating figure 5 from ...:

.. literalinclude:: fig5.py
.. image:: fig5.png
