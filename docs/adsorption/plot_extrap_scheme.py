# creates: RPA_conv.png
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt


def RPA_extrap(E1, E2, ecut1=400.0, ecut2=500.0):
    return np.linalg.solve(np.array([[1, 1 / (ecut1**(3 / 2))],
                                     [1, 1 / (ecut2**(3 / 2))]]),
                           np.array([E1, E2]))


ecut1 = np.array([300, 400, 500])
ecut2 = np.array([314, 381, 500])

E_k6_calc = [3.9795, 3.9430, 3.9262]
E_k12_calc = [4.1661]  # 300 eV
E_k12_predicted = [4.1296, 4.1128]  # 400, 500 eV

E_calc = [4.1580, 4.1347, 4.1176]  # 314, 381, 500

k6_extrap_1, k6_extrap_2 = RPA_extrap(E_k6_calc[1], E_k6_calc[2])
k12_extrap_1, k12_extrap_2 = RPA_extrap(E_calc[1], E_calc[2], ecut1=381.,
                                        ecut2=500.)
k12_extrap_predict_1, k12_extrap_predict_2 = RPA_extrap(E_k12_predicted[0],
                                                        E_k12_predicted[1])

xgrid = np.linspace(0, 1 / 400**(3 / 2), 1000)
xgrid2 = np.linspace(0, 1 / 381**(3 / 2), 1000)
plt.figure()

# Points
plt.plot(1 / ecut1**(3 / 2), E_k6_calc, 'ko', markersize=9,
         label=r'$6\times6\times1$ k-points, Calculation')
plt.plot(1 / ecut2**(3 / 2), E_calc, 'ro', markersize=9,
         label=r'$12\times12\times1$ k-points, Calculation')
plt.plot(1 / ecut1[0]**(3 / 2), E_k12_calc, 'go', markersize=9,
         label=r'$12\times12\times1$ k-points, Calculation')
plt.plot(1 / ecut1[1::]**(3 / 2), E_k12_predicted, 'gx', markersize=9, mew=3,
         label='Estimation')

# Extrap lines
plt.plot(xgrid, k6_extrap_1 + k6_extrap_2 * xgrid, 'k--', linewidth=2.5)
plt.plot(xgrid2, k12_extrap_1 + k12_extrap_2 * xgrid2, 'r--', linewidth=2.5)
plt.plot(xgrid, k12_extrap_predict_1 + k12_extrap_predict_2 * xgrid, 'g--',
         linewidth=2.5)

plt.annotate('Error: %.3f eV' % (k12_extrap_1 - k12_extrap_predict_1),
             (0, 4.08), (0.00004, 4.02),
             arrowprops=dict(arrowstyle='->'), fontsize=14)

plt.xlim([0, 1 / 280**(3 / 2)])
plt.ylim([3.75, 4.20])

plt.xticks([0, 1 / 500**(3 / 2), 1 / 400**(3 / 2), 1 / 300**(3 / 2)],
           [r'$\infty$', 500, 400, 300], size=16)
plt.yticks([3.80, 3.90, 4.00, 4.10, 4.20], size=16)

plt.legend(loc='lower right', prop={'size': 10}, numpoints=1)
plt.xlabel(r'1/$E_{\mathrm{cut}}^{3/2}$', size=22)
plt.ylabel(r'$E_c^{\mathrm{RPA}}$ (eV)', size=22)
plt.tight_layout()

plt.savefig('RPA_conv.png', dpi=500)
# plt.show()
