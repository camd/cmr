# creates: database_example.svg
import numpy as np
import matplotlib.pyplot as plt
import ase.db

adsorbate = 'NO'
slabs = ['Sc', 'Ti', 'Cu', 'Pd']
db = ase.db.connect('adsorption.db')
db_surf = ase.db.connect('surfaces.db')

labels = ['LDA', 'PBE', 'RPBE', 'BEEF-vdW', 'RPA']
markers = ['o', 's', 'v', 'D', 'x']
markersize = [10, 10, 10, 10, 12]
mews = [1, 1, 1, 1, 4]
cols = ['r', 'k', 'b', 'g']

plt.figure()
for ii, slab in enumerate(slabs):
    rows = db.select(adsorbate=adsorbate)
    adss = []
    for row in rows:
        if row.symbols[0] == slab:
            adss.append(row.LDA_adsorp)
            adss.append(row.PBE_adsorp)
            adss.append(row.RPBE_adsorp)
            adss.append(row.BEEFvdW_adsorp)
            adss.append(row.RPA_EXX_adsorp)

    rows_surf = db_surf.select(surf_mat=slab)
    surfs = []
    for row in rows_surf:
        surfs.append(row.LDA_surf)
        surfs.append(row.PBE_surf)
        surfs.append(row.RPBE_surf)
        surfs.append(row.BEEFvdW_surf)
        surfs.append(row.RPA_EXX_surf)

    for i in range(len(adss)):
        if ii == 0:
            plt.plot(surfs[i], adss[i], color='darkgray',
                     marker=markers[i], markersize=markersize[i],
                     mew=mews[i], label=labels[i])

        plt.plot(surfs[i], adss[i], color=cols[ii],
                 marker=markers[i], markersize=markersize[i], mew=mews[i])

    p = np.polyfit(surfs[:-1],
                   adss[:-1],
                   deg=1)

    plt.plot([surfs[2], surfs[0]],
             p[0] * np.array([surfs[2], surfs[0]]) + p[1],
             color=cols[ii])
    plt.annotate(slabs[ii], (surfs[-1] + 0.02, adss[-1]), color=cols[ii],
                 size=14)

plt.legend(loc='lower left', numpoints=1, prop={'size': 14})
plt.title('NO adsorption', size=18)

plt.ylim([-3.5, 0])
plt.xlim([0.3, 1.1])

plt.xlabel(r'$E_{\sigma}$ (eV)', size=22)
plt.ylabel(r'$E_{\mathrm{ads}}$ (eV)', size=22)

plt.xticks(size=18)
plt.yticks(size=18)
plt.tight_layout()
plt.savefig('database_example.svg', dpi=500)
