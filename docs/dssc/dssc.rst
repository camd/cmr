.. _dssc:

Porphyrin based dyes
====================

We present a computational screening study of more than 12,000 porphyrin-
based dyes obtained by modifying the porphyrin backbone (metal center and
axial ligands), substituting hydrogen by fluorine, and adding different side
and anchoring groups.

The first 5145 porphyrins are described in:

.. container:: article

    Kristian B. Ørnsø, Juan M. García-Lastra and Kristian S. Thygesen

    `Computational screening of functionalized zinc porphyrins for dye
    sensitized solar cells <http://dx.doi.org/10.1039/C3CP54050B>`_

    Phys. Chem. Chem. Phys., 2013, 15, 19478-19486

.. container:: article

    Kristian B. Ørnsø, Christian S. Pedersen, Juan M. García-Lastra and
    Kristian S. Thygesen

    `Optimizing porphyrins for dye sensitized solar cells using large-scale
    ab initio calculations <http://dx.doi.org/10.1039/C4CP01289E>`_

    Phys. Chem. Chem. Phys., 2014, 16, 16246-16254

Examples of using the database can be found in:

.. container:: article

    Kristian B. Ørnsø, Juan M. García-Lastra, Gema De La Torre,
    F. J. Himpsel, Angel Rubio and Kristian S. Thygesen

    `Design of two-photon molecular tandem architectures for solar
    cells by ab initio theory <http://dx.doi.org/10.1039/C4SC03835E>`_

    Chem. Sci., 2015, 6, 3018-3025

.. container:: article

    Kristian B. Ørnsø, Elvar Ö. Jónsson, Karsten W. Jacobsen and
    Kristian S. Thygesen

    `Importance of the Reorganization Energy Barrier in Computational
    Design of Porphyrin-Based Solar Cells with Cobalt-Based Redox
    Mediators <http://dx.doi.org/10.1021/JP512627E>`_

    J. Phys. Chem. C, 2015, 119, 12792-12800

.. contents::

* :download:`Download raw data <dssc.db>`
* `Browse data <https://cmrdb.fysik.dtu.dk/dssc>`_


Key-value pairs
---------------

.. figure:: mol.png

   Example of dye with: M=ZnP, A=EthynPhA, R1=Ph, R2=Ph, R3=Ph

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example of how to use the database to create Figure S1 in the 2014 paper
-------------------------------------------------------------------------

.. include:: homolumo.py
   :code: python

.. image:: homolumo.svg
   :width: 600

List of structure related keywords
----------------------------------

+---------------+---------------------+---------------------------+
|Metal centers  | Side groups         |  Anchor groups            |
+===============+=====================+===========================+
| ZnP           | Ph                  | EthynPhA                  |
+---------------+---------------------+---------------------------+
| FZnP          | FPh                 | 2CyanoPropenA             |
+---------------+---------------------+---------------------------+
| H2P           | DMP                 | 2CarboxyPropenA           |
+---------------+---------------------+---------------------------+
| FH2P          | TPA                 | EthenThPCyanoAcryl        |
+---------------+---------------------+---------------------------+
| TiOP          | MOTPA               | EthynBTDPhA               |
+---------------+---------------------+---------------------------+
| FTiOP         | TMP                 | EthynDPhEPhA              |
+---------------+---------------------+---------------------------+
| TiO2RP        | DTA                 | EthynPhEPhA               |
+---------------+---------------------+---------------------------+
| FTiO2RP       | DTBP                | EthynTPhEPhA              |
+---------------+---------------------+---------------------------+
|               | EthynPhM (only R2)  | EthynPhDA                 |
+---------------+---------------------+---------------------------+
|               |                     | EthynFuA                  |
+---------------+---------------------+---------------------------+
|               |                     | EthynThPCyanoAcryl        |
+---------------+---------------------+---------------------------+
|               |                     | EthynDThPCyanoAcryl       |
+---------------+---------------------+---------------------------+
|               |                     | DThPCyanoAcryl            |
+---------------+---------------------+---------------------------+
|               |                     | ThPCyanoAcryl             |
+---------------+---------------------+---------------------------+
|               |                     | EthynThPA                 |
+---------------+---------------------+---------------------------+
|               |                     | EthynDThPA                |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynPhA              |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthenThPCyanoAcryl    |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynBTDPhA           |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynDPhEPhA          |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynPhEPhA           |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynTPhEPhA          |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynPhDA             |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynFuA              |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynThPCyanoAcryl    |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynDThPCyanoAcryl   |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynThPA             |
+---------------+---------------------+---------------------------+
|               |                     | rot-EthynDThPA            |
+---------------+---------------------+---------------------------+
