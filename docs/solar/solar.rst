.. _solar:

Database of Organic donor-acceptor molecules
============================================

This database contains the Kohn-Sham (B3LYP) HOMO, LUMO, HOMO-LUMO gap,
singlet-triplet gap of organic donor-acceptor molecules. The database also
contains tight-binding extrapolated HOMO-LUMO gap for polymers and Voc for
PCBM blended solar cell with the corresponding molecule.

.. contents::


The data
--------

* Download database: :download:`solar.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/solar>`_


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1
