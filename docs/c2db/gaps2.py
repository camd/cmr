from pathlib import Path
import json

gw = []
hse = []
pbe = []
for path in Path().glob('materials/A*/*/*'):
    data = json.loads((path / 'data.json').read_text())
    if 'gap_gw' in data:
        gw.append(data['gap_gw'])
        hse.append(data['gap_hse'])
        pbe.append(data['gap'])
