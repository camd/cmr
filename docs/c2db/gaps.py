import matplotlib.pyplot as plt
from ase.db import connect

db = connect('c2db.db')
rows = db.select('gap_gw')

gw = []
hse = []
pbe = []
for row in rows:
    gw.append(row.gap_gw)
    hse.append(row.gap_hse)
    pbe.append(row.gap)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(pbe, hse, 'o', label='HSE06')
ax.plot(pbe, gw, 'x', label='GW')
x = [0, max(pbe)]
ax.plot(x, x, label='PBE')
ax.set_xlabel('PBE-gap [eV]')
ax.set_ylabel('gap [eV]')
ax.legend()
plt.savefig('gaps.png')
