# creates: epsMoS2.svg
import os
from pathlib import Path

import matplotlib.pyplot as plt
from qeh import Heterostructure

if not Path('chi-data').is_dir():
    os.system('tar -xf chi-data.tar.gz')
if not Path('H-MoS2-chi.npz').is_file():
    os.system('cp chi-data/H-MoS2-chi.npz .')

# Calculate static dielectric function for multilayer MoS2 for 1 to 20 layers:
for n in [1, 2, 3, 4, 5, 10, 20]:
    d = [6.15 for i in range(n - 1)]
    HS = Heterostructure(structure=['%dH-MoS2' % n],  # set up structure
                         d=d,                         # layer distance array
                         include_dipole=True,
                         wmax=0,                      # only include w=0
                         qmax=1,                      # q grid up to 1 Ang^{-1}
                         d0=6.15)                     # width of single layer
    q, w, epsM = HS.get_macroscopic_dielectric_function()
    plt.plot(q, epsM.real, label=' N = %s' % n)

plt.xlim(0, 1)
plt.xlabel(r'$q_\parallel (\mathrm{\AA^{-1}}$)', fontsize=20)
plt.ylabel(r'$\epsilon_M(q, \omega=0)$', fontsize=20)
plt.title('Static dielectric function', fontsize=20)
plt.legend(ncol=2, loc='best')
plt.savefig('epsMoS2.svg')
