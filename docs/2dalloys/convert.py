from ase.db import connect

db1 = connect('2dalloys.db')
with connect('2dalloysB.db') as db2:
    for row in db1.select():
        kvp = row.key_value_pairs
        kvp['prototype'] = {'1T': 'CdI2', '2H': 'MoS2'}[kvp.pop('phase')]
        kvp['nalloys'] = kvp.pop('ncomponents')
        db2.write(row, **kvp)
