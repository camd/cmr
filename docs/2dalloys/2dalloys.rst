.. _2dalloys:

=================
2D-alloy database
=================

If you are using data from this database in your research, please cite the
following paper:

.. container:: article

    `Activating the Basal Plane of 2D Transition Metal Dichalcogenides
    via High-Entropy Alloying`__

    Mohammad Amin Akhound, Karsten Wedel Jacobsen, Kristian Sommer Thygesen

    __ https://doi.org/10.1021/jacs.4c13863

    JCAS

    `ArXiv <https://arxiv.org/abs/2410.06876>`_
 


* `Browse data <https://cmrdb.fysik.dtu.dk/2dalloys>`_
* Download database: :download:`2dalloys.db`


.. contents::

Brief description
=================

The database contains atomic structures, formation enthalpy at T=0 K,
and free energy of formation at T=1000 K (including mixing entropy) for
400 monolayer transition metal dichalcogenide (TMDC) alloys in the 1T
and 2H phases.  For some structures, the binding energy of hydrogen on
the different adsorption sites are also available.  All calculations
were performed using density functional theory with the PBE
xc-functional.  The database is under development and will be expanded
in the near future to cover more types of 2D alloys.



Key-value pairs
===============

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 2
