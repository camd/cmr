.. _a2bcx4:

Database of A2BCX4 materials
============================

.. container:: article

    Mohnish Pandey and Karsten W. Jacobsen

    *Promising Quaternary Chalcogenides as High Band Gap Semiconductors for Tandem Photoelectrochemical Water Splitting Devices: A Computational Screening Approach*

.. contents::


The data
--------

* Download database: :download:`a2bcx4.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/a2bcx4>`_


Key-value pairs
---------------


.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example
-------

mBEEF energies and the corresponding uncertainties for six phases:

.. image:: enthalpy.png
.. literalinclude:: enthalpy.py
