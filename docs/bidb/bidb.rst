.. _bidb:

=====================================
van der Waals Bilayer Database (BiDB)
=====================================

If you are using data from this database in your research, please cite the
following paper:

.. container:: article

    `High-throughput computational stacking reveals emergent properties
    in natural van der Waals bilayers`__

    Sahar Pakdel, Asbjørn Rasmussen, Alireza Taghizadeh, Mads Kruse,
    Thomas Olsen, Kristian S. Thygesen

     Nat Commun **15**, 932 (2024)

    __ https://doi.org/10.1038/s41467-024-45003-w


* `Browse data <https://bidb.fysik.dtu.dk>`__
* Download data: :download:`bidb.db`

.. contents::


Brief description
=================

.. image:: bidb-workflow.png

Stacking of two-dimensional (2D) materials has emerged as a facile strategy
for realising exotic quantum states of matter and engineering electronic
properties.  Yet, developments beyond the proof-of-principle level are
impeded by the vast size of the configuration space defined by layer
combinations and stacking orders.  Here we employ a density functional
theory (DFT) workflow to calculate interlayer binding energies of 8451
homobilayers created by stacking 1052 different monolayers in various
configurations.  Analysis of the stacking orders in 247 experimentally
known van der Waals crystals is used to validate the workflow and determine
the criteria for realizable bilayers.  For the 2586 most stable bilayer
systems, we calculate a range of electronic, magnetic, and vibrational
properties, and explore general trends and anomalies.  We identify an
abundance of bistable bilayers with stacking order-dependent magnetic or
electrical polarisation states making them candidates for slidetronics
applications.


Key-value pairs
===============

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 2


Examples
========

...
