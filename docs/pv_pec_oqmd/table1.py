# creates: table1.csv
import ase.db

db = ase.db.connect('pv_pec_oqmd.db')

rows = list(db.select('0.1<=PBE_gap<=2'))
n1 = len(rows)
rows = [row for row in rows if not row.magnetic]
n2 = len(rows)
rows = [row for row in rows if 0.5 < row.GLLB_ind < 2.5]
n3 = len(rows)
rows = [row for row in rows
        if 'm_e' in row and row.m_e < 1 and row.m_h < 1]
n4 = len(rows)
rows = [row for row in rows if row.defect_tolerant]
n5 = len(rows)

assert (n1, n2, n3, n4, n5) == (1629, 929, 519, 222, 74)

with open('table1.csv', 'w') as fd:
    print('formula, GLLB_ind, GLLB_dir, m_e, m_h', file=fd)
    for row in sorted(rows, key=lambda row: row.GLLB_ind):
        print('{}, {:.2f}, {:.2f}, {:.2f}, {:.2f}'
              .format(row.formula,
                      row.GLLB_ind, row.GLLB_dir,
                      row.m_e, row.m_h), file=fd)
