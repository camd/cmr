Building this web-page
----------------------

Check out the source from https://gitlab.com/camd/cmr::

    $ git clone https://gitlab.com/camd/cmr.git
    $ cd cmr

Make sure you have up to date

* :ref:`ASE <ase:download_and_install>`
* Sphinx_

installations (``python3 -m pip install -U ase sphinx``).  Then do::

    $ cd docs/
    $ make

This will download the database files and run Python scripts to create images
and other stuff needed for the webpages (.svg and .csv files).  This will take
a couple of minutes the first time you do this.  After that, Sphinx_ will
build the html-files.

.. _Sphinx: http://www.sphinx-doc.org/en/master/


Modifying the pages
-------------------

Edit the ReST and Python files (.rst and .py files) and then run ``make``
again and check the results::

    $ make
    $ make browse

When things are OK, you can ``git commit`` the updated files and the
*cmr.fysik.dtu.dk* webpage will be updated automatically (happens once a day).
