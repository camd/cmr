=======================
How to filter materials
=======================

Example filter strings:

===========  ====================================
filter       meaning
===========  ====================================
``Cu>1``     more than 1 Cu atom
``gap>1.1``  "gap" larger than 1.1
``xc=PBE``   "xc" equal to "PBE"
``MoS2``     one or more MoS2 formula units
``Mo``       same as ``Mo>0`` or ``Mo!=0``
``gap``      material has key named ``gap``
===========  ====================================

Strings can be combined with "``,``" (and) and "``|``" (or).  Use "``~``"
for "not".  "Grouping can be done using "``(``" and "``)``":

================================  =====================================
filter                            meaning
================================  =====================================
``Au, O=0``                       contains gold but no oxygen
``~(Au=0 | O)``                   same as above
``Cu | Fe``                       contains Cu or Fe
``gap>1.1, (Cu | Fe)``            band gap larger than 1.1 and contains
                                  Cu or Fe
``(C|Si|Ge|Sn),Pb=0,nspecies=3``  contains C, Si, Ge or Sn, no Pb and 3
                                  species
``~gap``                          material has no key named ``gap``
================================  =====================================
