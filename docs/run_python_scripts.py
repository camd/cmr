import functools
from pathlib import Path
from urllib.request import urlretrieve

from ase.db import connect
from ase.utils.sphinx import create_png_files, git_role_tmpl, mol_role
from cmr import downloads

url = 'https://cmr.fysik.dtu.dk/'
repo = 'https://gitlab.com/camd/cmr/blob/master/'


def download():
    """Download the big data files and front-page images for each project."""
    for project, filenames in downloads:
        for filename in filenames:
            path = Path(project) / filename
            if not path.is_file():
                print('Downloading', path)
                if filename.endswith('.png'):
                    urlretrieve(f'{url}/_images/{filename}', path)
                else:
                    urlretrieve(f'{url}/_downloads/{filename}', path)
        path = Path('static') / f'{project}.png'
        if not path.is_file():
            print('Downloading', path)
            urlretrieve(f'{url}/_static/{project}.png', path)


def create_test_database():
    cmr = Path('cmr.db')
    if cmr.is_file():
        return
    db = connect(cmr)
    for project, filenames in downloads:
        for filename in filenames:
            if filename.endswith('.db'):
                path = Path(project) / filename
                db2 = connect(path)
                for row in db2.select(limit=1):
                    db.write(row, project_name=filename[:-3], data=row.data,
                             **row.get('key_value_pairs', {}))
                if project == 'c2db':
                    db.metadata = db2.metadata


def setup(app):
    """Sphinx entry point."""
    app.add_role('mol', mol_role)
    app.add_role('git', functools.partial(git_role_tmpl, repo))
    download()
    create_png_files()
    create_test_database()


if __name__ == '__main__':
    download()
