.. _funct_perovskites:

Functional Perovskites
======================

We investigate the band gaps and optical spectra of functional perovskites
composed of layers of the two cubic perovskite semiconductors BaSnO3-BaTaO2N
and LaAlO3-LaTiO2N.

.. container:: article

    Ivano E. Castelli, Mohnish Pandey, Kristian S. Thygesen, and
    Karsten W. Jacobsen

    `Bandgap Engineering of Functional Perovskites Through Quantum
    Confinement and Tunneling`__

    Phys. Rev. B 91, 165309.

    __ http://dx.doi.org/10.1103/PhysRevB.91.165309

* :download:`Download raw data <funct_perovskites.db>`
* `Browse data <https://cmrdb.fysik.dtu.dk/funct_perovskites>`_


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Band gaps
---------

Here, we plot the band gaps at `\Gamma` when n A-layers is stuck with
n B-layers (with n between 1 and 6). The changes in the gaps can be
understood in terms of quantum confinement and tunneling. The cubic
perovskites selected as building blocks are BaSnO3 (A) with BaTaO2N (B)
or LaAlO3 (A) with LaTiO2N (B).

.. literalinclude:: figure.py

.. image:: gaps.svg
