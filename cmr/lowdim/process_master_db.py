import argparse
import collections
from ase.db import connect


# The currently permitted warning strings.  We don't want spelling mistakes or
# rubbish creeping into the database.  The list can be extended as needed.
# Please choose a sensible string, lower case ASCII letters only.
permitted_warnings = [
    '"missing hydrogen"',
    '"high pressure"',
    '"invalid structure"',
    '"partial structure"',
    '"theoretical structure"']


def get_warning_strings(warnings_path):
    lines = open(warnings_path).read().split('\n')
    lines = [e for e in lines if len(e) > 1]

    global permitted_warnings
    warnings = collections.defaultdict(list)

    for line in lines:
        line = line.lstrip().rstrip()
        line = line.split(',')
        dbid = int(line[0])
        for w in line[1:]:
            assert w in permitted_warnings
            warnings[dbid].append(w)

    return {dbid: ','.join(values) for dbid, values in warnings.items()}


def process(input_path, warnings_path, output_path, strip=True):
    warnings = get_warning_strings(warnings_path)
    db = connect(input_path)
    output_db = connect(output_path, append=False)

    seen = set()
    rows = db.select()
    for r in rows:

        # check for duplicates (shouldn't happen at this stage)
        if r.dbid in seen:
            continue
        seen.add(r.dbid)

        # strip information from ICSD entries if option selected
        a = r.toatoms()
        kvp = r.key_value_pairs
        if strip and r.source.lower() == 'icsd':
            a.positions[:] = 0
            a.cell = (1, 1, 1)
            r.doi = ''
            r.publication = ''

        if strip:
            kvp = {k: v for k, v in kvp if k != 'similar'}

        if r.dbid in warnings:
            kvp['warning'] = warnings[r.dbid]

        output_db.write(a, key_value_pairs=kvp)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Add warnings and '
        '(optionally) strip information from ICSD entries')
    parser.add_argument("masterfile", help="The master database")
    parser.add_argument("warningsfile", help="The warnings list")
    parser.add_argument("outputfile", help="The output database")
    parser.add_argument('--strip', action='store_true', default=False,
                        help='Strip ICSD entries')
    args = parser.parse_args()

    process(args.masterfile, args.warningsfile, args.outputfile, args.strip)
