"""Command line tools for handling c2db.

Run this script with "python c2db.py" and read the output for additional help.
"""

import os
import warnings
from pathlib import Path


from ase.io import read
from ase.visualize import view
from asr.core import (read_file, read_json, decode_json, dct_to_result,
                      chdir, ASRResult, write_file)
from asr.core.results import HackedASRResult
import click
from gpaw import GPAW


def show_structures(folders):
    """Show atomic structures."""
    list_of_atoms = []

    for folder in folders:
        list_of_atoms.append(read(folder / 'structure.json'))
    view(list_of_atoms)


def move_folders(folders,
                 move_to='excluded_tree',
                 move_from='tree',
                 dry_run=False):
    """Move folder between trees.."""
    for folder in folders:
        new_folder = Path(str(folder).replace(move_from,
                                              move_to))
        assert not new_folder.is_dir(), \
            f'Folder={new_folder} already exists.'
        if dry_run:
            print(f'    Would move {folder} to {new_folder}.')
        else:
            print(f'    Moving {folder} to {new_folder}.')
            parent = new_folder.parent
            os.makedirs(parent, exist_ok=True)
            folder.rename(new_folder)


def interactive_exclude(folders, move_to='excluded_tree'):
    """Interactively show and maybe move folder to excluded materials."""
    action = None
    print(f'Interactive exclusion of {len(folders)} materials.')
    print(f'Target folder: {move_to}')
    for folder in folders:
        print(f'Going to exclude: {folder}')

    while action != '':
        action = input("'s': show structures. "
                       "'exclude': exclude structures. "
                       "'': dont do anything. ")

        if action == 's':
            show_structures(folders)
        elif action == 'exclude':
            move_folders(folders, move_to=move_to, move_from='tree')
            action = ''


@click.group()
def cli():
    """CLI interface for handling the C2DB folder tree.

    This script defines functions for handling the C2DB folder tree and
    implements functionality for excluding materials of other dimensionalities,
    excluding materials that have relaxed into multilayer structures and for
    excluding duplicates.

    To see the help and detailed description of any of these sub-commands,
    i.e. for exclude-duplicates do "python c2db.py exclude-duplicates --help"

    When a material is excluded from the working tree "tree/", it will be
    placed into a "excluded_*" folder at the same location where "*"
    depends on the exclusion algorithm. For example, say that a material
    "tree/A/AB2/MoS2/MoS2-12345678" has been excluded, then it will be placed
    in the equivalent folder "excluded_*/A/AB2/MoS2/MoS2-12345678". In this way
    we never throw out a material. The default behavior of the exclusion
    sub-commands is to not do anything, but when run they offer an interactive
    prompt for showing the targeted materials before exclusion such that the
    user will be able to manually inspect the materials before any action is
    taken. The command prompt takes three options: 's' to show targeted
    structures, 'exclude' to exclude targeted structures and '' (blank, the
    results of just pressing RETURN) to not do anything.

    """


# XXX note that this is a *string* not a tuple!
ignorefiles = ','.join(
    ['results-asr.bandstructure_ml.json',
     'results-asr.hse.bugged.json',
     'results-asr.gw.bugged.json',
     'results-asr.phonopy.json',
     'results-asr.structureinfo_large_vacuum.json',
     'results-asr.database.material_fingerprint_large_vacuum.json',
     'results-asr.plasmafrequency@calculate.json',
     'results-asr.new_emasses.json'])


def all_tree_folder_globs():
    return [
        'tree/A*/*/*/',
        'ICSD-COD/*el/*/',
        'adhoc_materials/*/',
        'tree_LDP/A*/*/*/',
        'tree_CDVAE/A*/*/*/',
        'tree_intercalated/A*/*/*/',
        'push-manti-tree/A*/*/*/',
        '/home/niflheim2/pmely/trees_to_collect/tree_Wang23/A*/*/*/',
    ]


@cli.command()
@click.option(
    '--dbname',
    help='write database to this path')
@click.argument('glob_patterns', nargs=-1)
def collect(dbname, glob_patterns):
    """Collect a new C2DB database without child folders.

    That is, only first class materials.
    """
    from asr.database.fromtree import main as fromtree
    from datetime import datetime

    if not glob_patterns:
        glob_patterns = list(str(x) for x in all_tree_folder_globs())

    now = datetime.now()
    if dbname is None:
        dbname = (f'collected-databases/c2db-first-class-{now.year}'
                  f'{now.month:02d}{now.day:02d}.db')
    dbname = Path(dbname)
    assert not dbname.is_file(), \
        (f'{dbname} already exists! You cannot collect '
         'two databases on the same day.')
    fromtree(
        folders=glob_patterns,
        children_patterns='',
        exclude_patterns=ignorefiles,
        dbname=str(dbname),
        collection_hook=collection_hook,
        njobs=15,
    )


def collection_hook(rowinput):
    """Database collection hook.

    This is called for each row during database collection.
    We use it to add custom C2DB key/value pairs."""

    kvps = rowinput.key_value_pairs

    # C2DB nowadays likes "dynamically stable" to be a single boolean.
    stab1 = kvps.get('dynamic_stability_phonons')
    stab2 = kvps.get('dynamic_stability_stiffness')
    dynamically_stable = stab1 == 'high' and stab2 == 'high'
    kvps['dynamically_stable'] = dynamically_stable

    nspecies = len(set(rowinput.atoms.symbols))
    kvps['nspecies'] = nspecies


@cli.command()
def collect_reference_database():
    """Collect a new C2DB reference database."""
    from ase.db import connect
    from asr.database.fromtree import main as fromtree
    from datetime import datetime
    now = datetime.now()
    dbname = (f'collected-databases/references-c2db-{now.year}'
              f'{now.month:02d}{now.day:02d}.db')
    assert not Path(dbname).is_file(), \
        (f'{dbname} already exists! You cannot collect '
         'two databases on the same day.')
    fromtree(
        folders=all_tree_folder_globs(),
        recursive=False,
        patterns=('results-asr.magnetic_anisotropy.json,'
                  'results-asr.structureinfo.json,'
                  'results-asr.database.material_fingerprint.json,'
                  'results-asr.gs.json'),
        children_patterns='',
        njobs=10,
        dbname=dbname)
    metadata = {'title': 'Monolayers (from C2DB)',
                'legend': 'Monolayers',
                'name': '{row.formula} ({row.crystal_type})',
                'label': '{row.formula} ({row.crystal_type})',
                'link': '/c2db/row/{row.uid}',
                'method': 'DFT'}
    db = connect(dbname)
    db.metadata = metadata


@cli.command()
def locate_bad_results():
    result_files = Path('tree').glob('*/*/*/results-asr.*.json')
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        for result_file in result_files:
            text = read_file(result_file)
            dct = decode_json(text)
            result = dct_to_result(dct)
            if issubclass(ASRResult, type(result)):
                missing_keys = result.get_missing_keys()
                unknown_keys = result.get_unknown_keys()
                print(missing_keys)
                print(unknown_keys)


@cli.command()
def run_convex_hull():
    """Rerun convex hull for all materials."""
    from asr.core import chdir
    from asr.convex_hull import main
    from time import time

    t0 = time()
    paths = []
    paths += Path().glob('tree/*/*/*/structure.json')
    paths += Path().glob('ICSD-COD/*el/*/structure.json')
    paths += Path().glob('adhoc_materials/*/structure.json')

    referencedatabases = [
        '/home/niflheim2/cmr/databases/referencedatabases/oqmd123.db',
        '/home/niflheim2/cmr/C2DB-ASR/collected-databases/references-c2db.db'
    ]

    for path in paths:
        # Exclude the single inconsistent file (missing 'energy' key) for now:
        if 'Li2As2F24Xe6' in str(path):
            continue
        folder = path.parent
        now = time() - t0
        print(folder, 'Time elapsed:', now)
        with chdir(folder):
            main(databases=referencedatabases)


def _run_gs(path):
    folder = path.parent
    print(folder)
    with chdir(folder):
        old_res = read_json('results-asr.gs@calculate.json')
        if isinstance(old_res, dict):
            old_res['__asr_hacked__'] = True
            old_res = dct_to_result(old_res)
            assert isinstance(old_res, HackedASRResult)
            calc = GPAW('gs.gpw')
            metadata = old_res.metadata.todict()
            metadata['params'] = {'calculator': calc.parameters,
                                  'name': 'gpaw'}
            metadata['asr_name'] = 'asr.gs@calculate'
            res = ASRResult(data=old_res.data, metadata=metadata)
            fname = 'results-asr.gs@calculate.json'
            fullpath = folder / fname
            print(f'Writing {fullpath}')
            write_file(fname, f'{res:json}')
        elif isinstance(old_res, ASRResult):
            metadata = old_res.metadata.todict()
            if 'calculator' not in metadata['params']:
                calc = GPAW('gs.gpw')
                metadata['params'] = {'calculator': calc.parameters,
                                      'name': 'gpaw'}
                metadata['asr_name'] = 'asr.gs@calculate'
                res = ASRResult(data=old_res.data, metadata=metadata)
                fname = 'results-asr.gs@calculate.json'
                fullpath = folder / fname
                print(f'Rewriting hacked {fullpath}')
                write_file(fname, f'{res:json}')


@cli.command()
@click.option('--nprocs', default=10, type=int)
def run_gs_calculate(nprocs):
    """Convert old gs.gpw files into results-asr.gs@calculate.json."""
    import multiprocessing
    paths = Path().glob('tree/*/*/*/gs.gpw')

    with multiprocessing.Pool(processes=nprocs) as pool:
        pool.map(_run_gs, paths)


@cli.command()
@click.argument('database')
def test_database_app(database):
    """Try to generate all figures of webpanel."""
    from asr.database.app import main as app
    app(databases=[database], test=True)


@cli.command()
@click.argument('recipe_name')
@click.option('--must-exist', default=None, type=str,
              help=('Name of file that must exist to run recipe. '
                    'Defaults to results-recipe_name.json'))
def update_results(recipe_name, must_exist):
    """Update results where there already exists.

    I.e. "p c2db.py update-results asr.phonons" will look for all
    folders where "results-asr.phonons.json already exists and run
    asr.phonons again in those folders, thus updating the results to
    the newest version.

    """
    from asr.core import get_recipe_from_name, chdir

    recipe = get_recipe_from_name(recipe_name)
    if must_exist:
        look_for_file = must_exist
    else:
        resultfile = f'results-{recipe_name}.json'
        look_for_file = resultfile

    paths = Path().glob(f'tree/*/*/*/{look_for_file}')
    for path in paths:
        folder = path.parent
        print(f'Running {recipe_name} in {folder}')
        with chdir(folder):
            recipe()


@cli.command()
@click.argument('folders', nargs=-1)
@click.option('--dry-run', is_flag=True)
def reinclude_folders(folders, dry_run=False):
    """Merged excluded tree with the C2DB tree once again."""
    tree_name = str(folders[0]).split('/')[0]
    assert 'tree' in tree_name, 'Not a valid tree'
    print(f'tree_name={tree_name}')

    folders = [Path(folder) for folder in folders]

    for folder in folders:
        assert str(folder).startswith(tree_name), \
            f'folder={folder} not contained in tree={tree_name}.'
    move_folders(folders, move_from=tree_name, move_to='tree', dry_run=dry_run)


@cli.command()
def exclude_non_2D_structures():
    """Exclude non 2D structures.

    This function excludes materials that are not consisting of a single 2D
    layer. This check is being performed by using the
    ase.geometry.dimensionality module to find the critical thresholds where
    the dimensionality changes. If the critical dimensionality where a material
    changes to a pure 2D material with only one 2D cluster is > 1.35 then we
    say that the material is not a single 2D layer. This could happen if the
    material dis-integrated during relaxation into multiple 2D layers or a
    combination of 0D, 1D, 2D layers. These materials are then placed in a
    separate tree: "excluded_non_2D_tree".

    """
    print('Looking for multilayer structures...')
    from pathlib import Path
    paths = Path().glob('tree/*/*/*/results-asr.dimensionality.json')

    folders = {}
    for path in paths:
        dim_results = read_json(path)
        k_intervals = dim_results['k_intervals']
        k_intervals = sorted(k_intervals, key=lambda item: item['a'])
        relevant_interval = k_intervals[-1]

        if relevant_interval['a'] > 1.35:
            folder = path.parent
            folders[folder] = relevant_interval['a']

    folders = {key: value for key, value in sorted(folders.items(),
                                                   key=lambda item: item[1])}
    if folders:
        nmat = len(folders)
        msg = (f'Found {nmat} multilayer 2D materials materials. '
               'These materials should probably be excluded.')
        print(msg)
        for folder, threshold in folders.items():
            print(f'    {folder} interval_threshold={threshold:.2f}')
        interactive_exclude(folders.keys(), move_to='excluded_non_2D_tree')


def update_rmsd_results(dbname):
    """Collect materials from tree/ and calculate new rmsd."""
    from asr.database.fromtree import main as fromtree

    update_files = [dbname,
                    'results-asr.database.rmsd.json',
                    'results-asr.database.duplicates.json']
    for filename in update_files:
        path = Path(filename)
        if path.is_file():
            path.unlink()
    relevant_files = ['results-asr.gs.json',
                      'results-asr.database.material_fingerprint.json',
                      'results-asr.magstate.json',
                      'results-asr.convex_hull.json']
    fromtree(folders=['tree/*/*/*/'], children_patterns='',
             patterns=','.join(relevant_files),
             dbname=dbname)


def handle_duplicate_results(dbname, duplicate_results,
                             move_to='excluded_tree'):
    """Read results from duplicates recipe and print excluded materials."""
    from ase.db import connect
    duplicate_uids = duplicate_results['duplicate_uids']
    if duplicate_uids:
        nduplicates = len(duplicate_uids)
        duplicate_groups = duplicate_results['duplicate_groups']
        ngroups = len(duplicate_groups)
        print(f'Found {nduplicates} materials that could be excluded.')
        print(f'{ngroups} duplicate groups.')
        print('Would suggest excluding:')
        db = connect(dbname)
        folders = []
        for group in duplicate_groups:
            excludes = [uid for uid in group['exclude']]
            for uid in excludes:
                row = db.get(uid=uid)
                folders.append(Path(row.folder))
        interactive_exclude(folders, move_to=move_to)


@cli.command()
@click.option('--update-rmsd/--dont-update-rmsd',
              is_flag=True, default=False, show_default=True)
def exclude_non_pareto_optimal_polymorphs(update_rmsd):
    """Exclude non pareto optimal polymorphs.

    This recipe excludes materials which are unimportant to include in
    the C2DB. The exclusion is based on two principles, first we
    cluster all chemically and structurally similar materials, and
    then we compute the (N,E)-pareto optimal materials and only keep
    those.

    Concretely, a material is unimportant if there exists another
    material with the same stoichiometry, chemical elements, within a
    distance of 0.3Å, lower HOF AND has fewer/equal number of atoms
    (phew, that was a lot of conditions).

    In other words, Mo2S4 would be excluded if there exists another
    material, like MoS2, that has lower energy.

    One could have removed all but the most stable material within
    every polymorph but it might actually be interesting to keep some
    materials that arent the most stable. For example, it makes sense
    that materials with fewer atoms have larger HOF than materials
    with more atoms since the former are restricted into a higher
    symmetry group. The converse do not make sense and we remove such
    materials as explained above.

    The excluded materials are put into "excluded_polymorphs_tree".

    """
    from asr.database.duplicates import main as calculate_duplicates
    dbname = 'rmsd.db'

    if update_rmsd:
        update_rmsd_results(dbname)

    duplicate_results = calculate_duplicates(
        dbname,
        filterstring='<=natoms,<hform')

    handle_duplicate_results(dbname, duplicate_results,
                             move_to='excluded_polymorphs_tree')


@cli.command()
@click.option('--update-rmsd/--dont-update-rmsd',
              is_flag=True, default=False, show_default=True)
def exclude_spinpolarized_duplicates(update_rmsd):
    """Exclude spin polarized duplicate materials.

    This recipe excludes spin-polarized materials that are duplicates of a
    non-spin-polarized material. Duplicates are found using the
    asr.database.duplicates recipe with a threshold of 0.001 which in practice
    seems right.

    The excluded materials are put into "excluded_spinpol_duplicates_tree".

    """
    from asr.database.duplicates import main as calculate_duplicates
    dbname = 'rmsd.db'

    if update_rmsd:
        update_rmsd_results(dbname)

    duplicate_results = calculate_duplicates(
        dbname,
        filterstring='<nspins',
        rmsd_tol=0.001)

    handle_duplicate_results(dbname, duplicate_results,
                             move_to="excluded_spinpol_duplicates_tree")


@cli.command()
@click.option('--dry-run', is_flag=True)
def exclude_afm_materials(dry_run=False):
    """Exclude anti-ferromagnetic materials.

    We have decided not to have AFM materials in C2DB, due to them
    most often being frustrated and thus not representing the true
    ground state.

    """
    magstate_filename = 'results-asr.magstate.json'

    folders = []
    for filename in Path('.').glob(f'tree/*/*/*/{magstate_filename}'):
        magstateresults = read_json(filename)
        magstate = magstateresults['magstate']
        if magstate == 'AFM':
            folders.append(filename.parent)

    nfolders = len(folders)
    print('AFM folders:')
    for folder in folders:
        print(folder)
    print(f'nfolders={nfolders}')
    move_folders(folders,
                 move_to='excluded_AFM_tree',
                 move_from='tree',
                 dry_run=dry_run)


@cli.command()
@click.option('--update-rmsd/--dont-update-rmsd',
              is_flag=True, default=False, show_default=True)
def exclude_irrelevant_magstates(update_rmsd):
    """Exclude magstates with higher energy.

    I.e. if two materials with same number of atoms and small distance
    between them are found, then keep the magnetic state with lowest
    energy and exclude the high energy magnetic state.

    """
    from asr.database.duplicates import main as calculate_duplicates
    dbname = 'rmsd.db'

    if update_rmsd:
        update_rmsd_results(dbname)

    # "NM" > "FM" > "AFM"
    duplicate_results = calculate_duplicates(
        dbname,
        filterstring='<=natoms,<hform')

    handle_duplicate_results(dbname, duplicate_results,
                             move_to='excluded_irrelevant_magstates_tree')


@cli.command()
@click.argument('filename')
@click.option(
    '--update', is_flag=True,
    help='Update ids if they already exist.')
def add_icsd_cod_ids(filename, update):
    """Add ICSD and/or COD id's."""

    from asr.core import read_json, write_json
    text = Path(filename).read_text()
    lines = text.split('\n')

    data = []
    for line in lines:
        items = line.split(" ")
        assert len(items) == 3, items
        uid, label, id = items
        assert label.endswith(':')
        label = label.strip(':').lower()
        data.append((uid, label, int(id)))

    print('Number of IDs', len(data))
    for datum in data[20:]:
        print(datum)

    from ase.db import connect
    db = connect('collected-databases/c2db.db')

    for datum in data:
        uid, label, id = datum
        row = db.get(uid=uid)
        folder = row.folder
        print(folder)
        infojson = Path(folder) / 'info.json'
        if infojson.is_file():
            info = read_json(infojson)
        else:
            info = {}
        label_id = label + '_id'
        if label_id in info:
            if id == info[label_id]:
                continue
            elif not update:
                print(f'{label_id} already exists. '
                      'Use --update to update existing ids.')
                continue
        info[label_id] = id

        print(f'Adding {label_id}={id}')
        write_json(infojson, info)


if __name__ == '__main__':
    cli()
