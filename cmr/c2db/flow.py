"""Workflow for C2DB.

Important utility functions in this script:

  is_thermodynamically_stable
  is_dynamically_stablex

The workflow is factored into four components:

  basic_workflow
  dynamical_stability_workflow
  property_workflow

"""
import os
from pathlib import Path

import asr.emasses
from ase.io import read
from asr.core import read_json
from asr.setup.strains import get_relevant_strains, get_strained_folder_name
from asr.setup.strains import main as setupstrains
from myqueue.workflow import resources, run

VERBOSE = os.environ.get('MQVERBOSE', False)


class MissingUnrelaxedStructure(Exception):
    """Exception raised when unrelaxed.json is missing."""

    pass


def is_thermodynamically_stable():
    """Determine whether a material is thermodynamically stable."""
    ch = read_json('results-asr.convex_hull.json')
    hform = ch['hform']

    if hform > 0.2:
        return False
    return True


def is_dynamically_stable():
    """Determine whether a material is dynamically stable."""
    ph = read_json('results-asr.phonons.json')
    if ph['dynamic_stability_phonons'] == 'low':
        return False

    st = read_json('results-asr.stiffness.json')
    if st['dynamic_stability_stiffness'] == 'low':
        return False

    return True


reference_dbs = [
    '/home/niflheim2/cmr/databases/referencedatabases/oqmd123.db',
    '/home/niflheim2/cmr/C2DB-ASR/collected-databases/references-c2db.db']


def module(mod, **kwargs):
    # kwargs['cores'] = min(kwargs.get('cores', 1), 4)
    return run(module=mod, **kwargs)


@resources(cores=1, tmax='10m')
def basic_workflow():
    """Generate tasks related finding most stable magnetic structures."""
    if not Path('structure.json').is_file():
        if not Path('unrelaxed.json').is_file():
            raise MissingUnrelaxedStructure
        with module('asr.relax', cores=40, tmax='20h'):
            si = module('asr.structureinfo')
    else:
        si = module('asr.structureinfo')

    with si:
        with module('asr.gs', cores=40, tmax='20h'):
            module('asr.magstate')
            ch = module('asr.convex_hull', args=reference_dbs)
            with module('asr.bandstructure', cores=40, tmax='5h'):
                module('asr.projected_bandstructure')
            module('asr.pdos', cores=40, tmax='5h')
            module('asr.bader', tmax='1h')

    return ch


def dynamical_stability_workflow():
    """Generate tasks related to determining dynamical stability."""
    verbose_print('Executing dynamical_stability_workflow().')

    if not setupstrains.done:
        setupstrains()

    relevant_strains = get_relevant_strains(pbc=[True, True, False])
    strainfolders = [
        Path(get_strained_folder_name(sign * 1, i, j,
                                      clamped=False)).resolve()
        for i, j in relevant_strains for sign in [-1, 1]]

    assert strainfolders, 'Missing strain folders.'

    strain_runs = [module('asr.relax',
                          folder=strainfolder,
                          cores=40, tmax='3h')
                   for strainfolder in strainfolders]

    module('asr.stiffness', deps=strain_runs)

    return module('asr.phonons', restart=3, cores=40, tmax='8h')


def property_workflow():
    """Generate tasks for various material properties."""
    verbose_print('Executing property_workflow()')
    atoms = read('structure.json')

    # @resources(diskspace=1)
    pol = module('asr.polarizability', cores=120, tmax='20h')

    gsresults = read_json('results-asr.gs.json')
    structureinfo = read_json('results-asr.structureinfo.json')

    gap = gsresults.get('gap')
    gap_nosoc = gsresults.get('gap_nosoc')

    topology_file = Path('results-asr.berry.json')
    if topology_file.is_file():
        topresult = read_json(topology_file)
        topology = topresult['Topology']
    else:
        topology = None

    gap_tolerance = 0.01

    if gap > gap_tolerance:
        with module('asr.emasses', cores=40, tmax='5h'):
            run(function=asr.emasses.validate)
    else:
        module('asr.fermisurface')

    if gap_nosoc > gap_tolerance:
        verbose_print('Has band gap.')
        module('asr.hse', cores=40, tmax='10h')

        # Topologically non-trivial materials are especially hard.
        # We filter these away in case the material have been found to
        # have a non-trivial topology.
        if topology in {'Trivial', None}:
            with module('asr.borncharges', cores=40, tmax='5h', restart=3):
                with pol:
                    module('asr.infraredpolarizability')
            if not structureinfo['has_inversion_symmetry']:
                module('asr.piezoelectrictensor',
                       cores=40, tmax='20h', restart=3)

        if len(atoms) < 5 and gap > 0.2:
            module('asr.gw', cores=120, tmax='48h')
    else:
        module('asr.plasmafrequency', cores=40, tmax='20h')


def verbose_print(*args):
    """Only print if VERBOSE."""
    if VERBOSE:
        print(*args)


def workflow():
    """Create MyQueue Task list for the C2DB workflow.

    Note that this workflow relies on the folder layout of C2DB so be
    careful.

    """
    ch = basic_workflow()

    if not ch.done:
        return

    if not is_thermodynamically_stable():
        verbose_print('Is not thermodynamically stable.')
        return

    phonons = dynamical_stability_workflow()

    if not phonons.done:
        verbose_print('Missing dynamic stability tasks.')
        return

    if not is_dynamically_stable():
        verbose_print('Is not dynamically stable.')
        return

    property_workflow()
