from pathlib import Path
import pytest
# from asr.database import connect
from ase.db import connect


@pytest.mark.skip
def test_hse_result(client):
    """Test asr.hse.Result object."""
    path = Path(__file__).parent / '../../docs/c2db/c2db.db'
    db = connect(path)
    for row in db.select():
        break

    record = row.cache.get(name="asr.c2db.hse:main")
    result = record.result

    print(result.kvbm)
    print(result.kcbm)
