# flake8: noqa
# type: ignore
import os
from ase.db import connect
import pickle
import numpy as np
from gpaw import *

db = connect('ads_24_11.db')

direc = '/home/niflheim2/psisc/PhD_adsorp/relax_structs/'

ecuts = np.array([350, 400, 425, 450, 475, 500, 530, 560, 600])
adsorbates = ['H', 'O', 'N', 'N2', 'CO', 'NO', 'CH', 'OH']
slabs = ['Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh', 'Pd', 'Ag', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au']
for i, ads in enumerate(adsorbates):
    if ads == 'H':
        mol = 'H2'
        mol2 = None
    elif ads == 'O':
        mol = 'O2'
        mol2 = None
    elif ads == 'N':
        mol = 'N2'
        mol2 = None
    elif ads == 'CH':
        mol = 'CH4'
        mol2 = 'H2'
    elif ads == 'OH':
        mol = 'H2O'
        mol2 = 'H2'
    else:
        mol = ads
        mol2 = None

    for j, slab in enumerate(slabs):
        if slab == 'Fe' or slab == 'Co' or slab == 'Ni':
            spin = 'newsetup_spinpol_gpw/'
        else:
            spin = 'newsetup_gpw/'
        atoms, calc = restart(direc+spin+ads+'_'+slab+'_vac5_k12_pwcutoff800.gpw',txt=None)

        ###DFT
        #LDA
        LDA_ads = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_LDA.pckl','rb'),encoding='bytes'))
        LDA_slab = float(pickle.load(open('data_to_cmr/'+slab+'_LDA.pckl','rb'),encoding='bytes'))
        LDA_mol = float(pickle.load(open('data_to_cmr/'+mol+'_LDA.pckl','rb'),encoding='bytes'))
        if not mol2 == None:
            LDA_mol2 = float(pickle.load(open('data_to_cmr/'+mol2+'_LDA.pckl','rb'),encoding='bytes'))
        else:
            LDA_mol2 = np.nan
        LDA_adsorp = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_LDA_adsorp.pckl','rb'),encoding='bytes'))
        #PBE
        PBE_ads = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_PBE.pckl','rb'),encoding='bytes'))
        PBE_slab = float(pickle.load(open('data_to_cmr/'+slab+'_PBE.pckl','rb'),encoding='bytes'))
        PBE_mol = float(pickle.load(open('data_to_cmr/'+mol+'_PBE.pckl','rb'),encoding='bytes'))
        if not mol2 == None:
            PBE_mol2 = float(pickle.load(open('data_to_cmr/'+mol2+'_PBE.pckl','rb'),encoding='bytes'))
        else:
            PBE_mol2 = np.nan
        PBE_adsorp = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_PBE_adsorp.pckl','rb'),encoding='bytes'))

        #RPBE
        RPBE_ads = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_RPBE.pckl','rb'),encoding='bytes'))
        RPBE_slab = float(pickle.load(open('data_to_cmr/'+slab+'_RPBE.pckl','rb'),encoding='bytes'))
#        print(RPBE_ads, RPBE_slab)
        RPBE_mol = float(pickle.load(open('data_to_cmr/'+mol+'_RPBE.pckl','rb'),encoding='bytes'))
        if not mol2 == None:
            RPBE_mol2 = float(pickle.load(open('data_to_cmr/'+mol2+'_RPBE.pckl','rb'),encoding='bytes'))
        else:
            RPBE_mol2 = np.nan
        RPBE_adsorp = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_RPBE_adsorp.pckl','rb'),encoding='bytes'))

        #BEEF-vdW
        BEEFvdW_ads = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_BEEF-vdW.pckl','rb'),encoding='bytes'))
        BEEFvdW_slab = float(pickle.load(open('data_to_cmr/'+slab+'_BEEF-vdW.pckl','rb'),encoding='bytes'))
#        print(BEEFvdW_ads, BEEFvdW_slab)
        BEEFvdW_mol = float(pickle.load(open('data_to_cmr/'+mol+'_BEEFvdW.pckl','rb'),encoding='bytes'))
        if not mol2 == None:
            BEEFvdW_mol2 = float(pickle.load(open('data_to_cmr/'+mol2+'_BEEFvdW.pckl','rb'),encoding='bytes'))
        else:
            BEEFvdW_mol2 = np.nan
        BEEFvdW_adsorp = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_BEEF-vdW_adsorp.pckl','rb'),encoding='bytes'))

        #mBEEF
        mBEEF_ads = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_mBEEF.pckl','rb'),encoding='bytes'))
        mBEEF_slab = float(pickle.load(open('data_to_cmr/'+slab+'_mBEEF.pckl','rb'),encoding='bytes'))
        mBEEF_mol = float(pickle.load(open('data_to_cmr/'+mol+'_mBEEF.pckl','rb'),encoding='bytes'))
        if not mol2 == None:
            mBEEF_mol2 = float(pickle.load(open('data_to_cmr/'+mol2+'_mBEEF.pckl','rb'),encoding='bytes'))
        else:
            mBEEF_mol2 = np.nan
        mBEEF_adsorp = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_mBEEF_adsorp.pckl','rb'),encoding='bytes'))

        #mBEEF-vdW
        mBEEFvdW_ads = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_mBEEF-vdW.pckl','rb'),encoding='bytes'))
        mBEEFvdW_slab = float(pickle.load(open('data_to_cmr/'+slab+'_mBEEF-vdW.pckl','rb'),encoding='bytes'))
        mBEEFvdW_mol = float(pickle.load(open('data_to_cmr/'+mol+'_mBEEFvdW.pckl','rb'),encoding='bytes'))
        if not mol2 == None:
            mBEEFvdW_mol2 = float(pickle.load(open('data_to_cmr/'+mol2+'_mBEEFvdW.pckl','rb'),encoding='bytes'))
        else:
            mBEEFvdW_mol2 = np.nan
        mBEEFvdW_adsorp = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_mBEEF-vdW_adsorp.pckl','rb'),encoding='bytes'))

        #vdW-DF2
        vdWDF2_ads = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_vdW-DF2.pckl','rb'),encoding='bytes'))
        vdWDF2_slab = float(pickle.load(open('data_to_cmr/'+slab+'_vdW-DF2.pckl','rb'),encoding='bytes'))
        vdWDF2_mol = float(pickle.load(open('data_to_cmr/'+mol+'_vdWDF2.pckl','rb'),encoding='bytes'))
        if not mol2 == None:
            vdWDF2_mol2 = float(pickle.load(open('data_to_cmr/'+mol2+'_vdWDF2.pckl','rb'),encoding='bytes'))
        else:
            vdWDF2_mol2 = np.nan
        vdWDF2_adsorp = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_vdW-DF2_adsorp.pckl','rb'),encoding='bytes'))

        ###EXX
        EXX_ads = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_EXX.pckl','rb'),encoding='bytes'))
        EXX_slab = float(pickle.load(open('data_to_cmr/'+slab+'_EXX.pckl','rb'),encoding='bytes'))
        EXX_mol = float(pickle.load(open('data_to_cmr/'+mol+'_EXX.pckl','rb'),encoding='bytes'))
        if not mol2 == None:
            EXX_mol2 = float(pickle.load(open('data_to_cmr/'+mol2+'_EXX.pckl','rb'),encoding='bytes'))
        else:
            EXX_mol2 = np.nan
        EXX_adsorp = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_EXX_adsorp.pckl','rb'),encoding='bytes'))

        ###RPA
        RPA_ads_extrap = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_extrap.pckl','rb'),encoding='bytes'))
        RPA_ads_ecut300_k6 = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_ecut300_k6.pckl','rb'),encoding='bytes'))
        RPA_ads_ecut400_k6 = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_ecut400_k6.pckl','rb'),encoding='bytes'))
        RPA_ads_ecut500_k6 = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_ecut500_k6.pckl','rb'),encoding='bytes'))
        RPA_ads_ecut300_k12 = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_ecut300_k12.pckl','rb'),encoding='bytes'))

        RPA_slab_extrap = float(pickle.load(open('data_to_cmr/'+slab+'_extrap.pckl','rb'),encoding='bytes'))
        RPA_slab_ecut300_k6 = float(pickle.load(open('data_to_cmr/'+slab+'_ecut300_k6.pckl','rb'),encoding='bytes'))
        RPA_slab_ecut400_k6 = float(pickle.load(open('data_to_cmr/'+slab+'_ecut400_k6.pckl','rb'),encoding='bytes'))
        RPA_slab_ecut500_k6 = float(pickle.load(open('data_to_cmr/'+slab+'_ecut500_k6.pckl','rb'),encoding='bytes'))
        RPA_slab_ecut300_k12 = float(pickle.load(open('data_to_cmr/'+slab+'_ecut300_k12.pckl','rb'),encoding='bytes'))

        RPA_mol_extrap = float(pickle.load(open('data_to_cmr/'+mol+'_extrap.pckl','rb'),encoding='bytes'))
#        RPA_mol_ecut350 = pickle.load(open('data_to_cmr/'+mol+'_350eV.pckl','rb'),encoding='bytes')
#        RPA_mol_ecut400 = pickle.load(open('data_to_cmr/'+mol+'_400eV.pckl','rb'),encoding='bytes')
        RPA_mol_ecut425 = float(pickle.load(open('data_to_cmr/'+mol+'_425eV.pckl','rb'),encoding='bytes'))
        RPA_mol_ecut450 = float(pickle.load(open('data_to_cmr/'+mol+'_450eV.pckl','rb'),encoding='bytes'))
        RPA_mol_ecut475 = float(pickle.load(open('data_to_cmr/'+mol+'_475eV.pckl','rb'),encoding='bytes'))
        RPA_mol_ecut500 = float(pickle.load(open('data_to_cmr/'+mol+'_500eV.pckl','rb'),encoding='bytes'))
        RPA_mol_ecut530 = float(pickle.load(open('data_to_cmr/'+mol+'_530eV.pckl','rb'),encoding='bytes'))
        RPA_mol_ecut560 = float(pickle.load(open('data_to_cmr/'+mol+'_560eV.pckl','rb'),encoding='bytes'))
        RPA_mol_ecut600 = float(pickle.load(open('data_to_cmr/'+mol+'_600eV.pckl','rb'),encoding='bytes'))

        if not mol2 == None:
            RPA_mol2_extrap = float(pickle.load(open('data_to_cmr/'+mol2+'_extrap.pckl','rb'),encoding='bytes'))
            RPA_mol2_ecut425 = float(pickle.load(open('data_to_cmr/'+mol2+'_425eV.pckl','rb'),encoding='bytes'))
            RPA_mol2_ecut450 = float(pickle.load(open('data_to_cmr/'+mol2+'_450eV.pckl','rb'),encoding='bytes'))
            RPA_mol2_ecut475 = float(pickle.load(open('data_to_cmr/'+mol2+'_475eV.pckl','rb'),encoding='bytes'))
            RPA_mol2_ecut500 = float(pickle.load(open('data_to_cmr/'+mol2+'_500eV.pckl','rb'),encoding='bytes'))
            RPA_mol2_ecut530 = float(pickle.load(open('data_to_cmr/'+mol2+'_530eV.pckl','rb'),encoding='bytes'))
            RPA_mol2_ecut560 = float(pickle.load(open('data_to_cmr/'+mol2+'_560eV.pckl','rb'),encoding='bytes'))
            RPA_mol2_ecut600 = float(pickle.load(open('data_to_cmr/'+mol2+'_600eV.pckl','rb'),encoding='bytes'))
        else:
            RPA_mol2_extrap = np.nan
            RPA_mol2_ecut425 = np.nan
            RPA_mol2_ecut450 = np.nan
            RPA_mol2_ecut475 = np.nan
            RPA_mol2_ecut500 = np.nan
            RPA_mol2_ecut530 = np.nan
            RPA_mol2_ecut560 = np.nan
            RPA_mol2_ecut600 = np.nan

        RPA_adsorp = float(pickle.load(open('data_to_cmr/'+ads+'_'+slab+'_RPA_adsorp.pckl','rb'),encoding='bytes'))

        if not mol2 == None:
            db.write(atoms, surf_mat=slab, adsorbate=ads, mol=mol, mol2=mol2,
                     LDA_ads=LDA_ads, LDA_slab=LDA_slab, LDA_mol=LDA_mol, LDA_mol2=LDA_mol2, LDA_adsorp=LDA_adsorp,
                     PBE_ads=PBE_ads, PBE_slab=PBE_slab, PBE_mol=PBE_mol, PBE_mol2=PBE_mol2, PBE_adsorp=PBE_adsorp,
                     RPBE_ads=RPBE_ads, RPBE_slab=RPBE_slab, RPBE_mol=RPBE_mol, RPBE_mol2=RPBE_mol2, RPBE_adsorp=RPBE_adsorp,
                     BEEFvdW_ads=BEEFvdW_ads, BEEFvdW_slab=BEEFvdW_slab, BEEFvdW_mol=BEEFvdW_mol, BEEFvdW_mol2=BEEFvdW_mol2, BEEFvdW_adsorp=BEEFvdW_adsorp,
                     mBEEFvdW_ads=mBEEFvdW_ads, mBEEFvdW_slab=mBEEFvdW_slab, mBEEFvdW_mol=mBEEFvdW_mol, mBEEFvdW_mol2=mBEEFvdW_mol2, mBEEFvdW_adsorp=mBEEFvdW_adsorp,
                     mBEEF_ads=mBEEF_ads, mBEEF_slab=mBEEF_slab, mBEEF_mol=mBEEF_mol, mBEEF_mol2=mBEEF_mol2, mBEEF_adsorp=mBEEF_adsorp,
                     vdWDF2_ads=vdWDF2_ads, vdWDF2_slab=vdWDF2_slab, vdWDF2_mol=vdWDF2_mol, vdWDF2_mol2=vdWDF2_mol2, vdWDF2_adsorp=vdWDF2_adsorp,
                     EXX_ads=EXX_ads, EXX_slab=EXX_slab, EXX_mol=EXX_mol, EXX_mol2=EXX_mol2, EXX_adsorp=EXX_adsorp,
                     RPA_ads_extrap=RPA_ads_extrap, RPA_slab_extrap=RPA_slab_extrap, RPA_mol_extrap=RPA_mol_extrap,
                     RPA_mol2_extrap=RPA_mol2_extrap, RPA_adsorp=RPA_adsorp, RPA_EXX_adsorp=RPA_adsorp+EXX_adsorp,
                     RPA_ads_ecut300_k6=RPA_ads_ecut300_k6, RPA_ads_ecut400_k6=RPA_ads_ecut400_k6, RPA_ads_ecut500_k6=RPA_ads_ecut500_k6,
                     RPA_ads_ecut300_k12=RPA_ads_ecut300_k12,
                     RPA_slab_ecut300_k6=RPA_slab_ecut300_k6, RPA_slab_ecut400_k6=RPA_slab_ecut400_k6, RPA_slab_ecut500_k6=RPA_slab_ecut500_k6,
                     RPA_slab_ecut300_k12=RPA_slab_ecut300_k12,
                     RPA_mol_ecut425=RPA_mol_ecut425,RPA_mol_ecut450=RPA_mol_ecut450,RPA_mol_ecut475=RPA_mol_ecut475,
                     RPA_mol_ecut500=RPA_mol_ecut500,RPA_mol_ecut530=RPA_mol_ecut530,RPA_mol_ecut560=RPA_mol_ecut560,
                     RPA_mol_ecut600=RPA_mol_ecut600,
                     RPA_mol2_ecut425=RPA_mol2_ecut425,RPA_mol2_ecut450=RPA_mol2_ecut450,RPA_mol2_ecut475=RPA_mol2_ecut475,
                     RPA_mol2_ecut500=RPA_mol2_ecut500,RPA_mol2_ecut530=RPA_mol2_ecut530,RPA_mol2_ecut560=RPA_mol2_ecut560,
                     RPA_mol2_ecut600=RPA_mol2_ecut600)
        else:
            db.write(atoms, surf_mat=slab, adsorbate=ads, mol=mol,
                     LDA_ads=LDA_ads, LDA_slab=LDA_slab, LDA_mol=LDA_mol, LDA_adsorp=LDA_adsorp,
                     PBE_ads=PBE_ads, PBE_slab=PBE_slab, PBE_mol=PBE_mol, PBE_adsorp=PBE_adsorp,
                     RPBE_ads=RPBE_ads, RPBE_slab=RPBE_slab, RPBE_mol=RPBE_mol, RPBE_adsorp=RPBE_adsorp,
                     BEEFvdW_ads=BEEFvdW_ads, BEEFvdW_slab=BEEFvdW_slab, BEEFvdW_mol=BEEFvdW_mol, BEEFvdW_adsorp=BEEFvdW_adsorp,
                     mBEEFvdW_ads=mBEEFvdW_ads, mBEEFvdW_slab=mBEEFvdW_slab, mBEEFvdW_mol=mBEEFvdW_mol, mBEEFvdW_adsorp=mBEEFvdW_adsorp,
                     mBEEF_ads=mBEEF_ads, mBEEF_slab=mBEEF_slab, mBEEF_mol=mBEEF_mol, mBEEF_adsorp=mBEEF_adsorp,
                     vdWDF2_ads=vdWDF2_ads, vdWDF2_slab=vdWDF2_slab, vdWDF2_mol=vdWDF2_mol, vdWDF2_adsorp=vdWDF2_adsorp,
                     EXX_ads=EXX_ads, EXX_slab=EXX_slab, EXX_mol=EXX_mol, EXX_adsorp=EXX_adsorp,
                     RPA_ads_extrap=RPA_ads_extrap, RPA_slab_extrap=RPA_slab_extrap, RPA_mol_extrap=RPA_mol_extrap,
                     RPA_adsorp=RPA_adsorp, RPA_EXX_adsorp=RPA_adsorp+EXX_adsorp,
                     RPA_ads_ecut300_k6=RPA_ads_ecut300_k6, RPA_ads_ecut400_k6=RPA_ads_ecut400_k6, RPA_ads_ecut500_k6=RPA_ads_ecut500_k6,
                     RPA_ads_ecut300_k12=RPA_ads_ecut300_k12,
                     RPA_slab_ecut300_k6=RPA_slab_ecut300_k6, RPA_slab_ecut400_k6=RPA_slab_ecut400_k6, RPA_slab_ecut500_k6=RPA_slab_ecut500_k6,
                     RPA_slab_ecut300_k12=RPA_slab_ecut300_k12,
                     RPA_mol_ecut425=RPA_mol_ecut425,RPA_mol_ecut450=RPA_mol_ecut450,RPA_mol_ecut475=RPA_mol_ecut475,
                     RPA_mol_ecut500=RPA_mol_ecut500,RPA_mol_ecut530=RPA_mol_ecut530,RPA_mol_ecut560=RPA_mol_ecut560,
                     RPA_mol_ecut600=RPA_mol_ecut600)

#        db.write(atoms, surf_mat=slab, adsorbate=ads, LDA=LDA[i,j], PBE=PBE[i,j], RPBE=RPBE[i,j], BEEFvdW=BEEFvdW[i,j], mBEEF=mBEEF[i,j], mBEEFvdW=mBEEFvdW[i,j], vdWDF2=vdWDF2[i,j], RPA=RPA[i,j])

