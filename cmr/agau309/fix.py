import os
from ase.db import connect
from ase.calculators.singlepoint import SinglePointCalculator

os.environ['USER'] = 'pmla'

# db1 = connect('sorted.db')
db1 = connect('old.db')
db2 = connect('agau309.db')

for row in db1.select():
    atoms = row.toatoms()
    atoms.center(vacuum=3)
    atoms.calc = SinglePointCalculator(atoms, energy=row.energy)
    atoms.calc.name = 'asap'
    db2.write(atoms)
