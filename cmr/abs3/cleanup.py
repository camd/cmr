from ase.db import connect

db1 = connect('abs3-orig.db')
with connect('abs3.db') as db2:
    for row in db1.select():
        row.__dict__['energy'] = float(row.total_en)
        kvp = row.key_value_pairs
        del kvp['total_en']
        for key in ['E_hull', 'E_relative_perAtom', 'E_uncertanty_hull',
                    'E_uncertanty_perAtom', 'GLLB_dir', 'GLLB_ind',
                    'PBEsol_gap', 'm_e', 'm_h']:
            if not isinstance(kvp[key], float):
                kvp[key] = float(kvp[key])
        d = row.get('data')
        if 'bse_energy' in d:
            data = {'bse_energy': d.bse_energy,
                    'bse_ensemble': d.bse_ensemble}
        else:
            data = {}
        if 'xbs_k' in d:
            data.update({'x': d['xbs_k'],
                         'y': d['ebs_k'],
                         'X': d.bs['xlabel'][0],
                         'names': d.bs['xlabel'][1]})
        db2.write(row, data=data, **kvp)
